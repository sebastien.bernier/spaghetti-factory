# -*- coding: utf-8 -*-
"""
Created on Tue Jul 12 09:19:31 2016

@author: sebastien
"""
import matplotlib.pyplot as plt
import numpy as np
mi = float(input('Lower bound: x = '))
ma = float(input('Upper bound: x = '))
if ma < mi:
    print('Upper bound is smaller than lower bound. Just FYI.')
    ma, mi = mi, ma
n = int(input('Number of trapezoids: n = '))
x = np.linspace(mi, ma, n+1)
# Define your function here
y = np.log(x)*2
plt.plot(x, y)
sumy = np.float(y[0])+2*np.sum(y[1:n])+np.float(y[n])
a = ((ma-mi)/(2*n))*sumy  # Applying the trapezoid rule...
print('Area under the curve: a =', a)
