# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 14:16:10 2016

@author: sebastien
"""
import matplotlib.pyplot as plt
import numpy as np
mi = float(input('Lower bound: x = '))
ma = float(input('Upper bound: x = '))
n = float(input('Number of points: n = '))
x = np.linspace(ma, mi, n)
# Define your function here
y = 1/x
# Calculating dy/dx
dy = y[1:]-y[:-1]
dx = x[1:]-x[:-1]
dy_dx = dy/dx
X = (x[1:]+x[:-1])/2
# Plotting derivative
plt.plot(x, y, 'b', X, dy_dx, 'g')
