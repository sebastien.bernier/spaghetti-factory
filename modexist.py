# -*- coding: utf-8 -*-
"""
Created on Thu Jul  7 09:13:18 2016

@author: sebastien
"""
module_name = str(input('Module name : '))


def module_exists(module_name):
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True
# Defines a simple function that determines if a module exists or not
print(module_exists(module_name))
